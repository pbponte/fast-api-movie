from pydantic import BaseModel, Field
from typing import Optional

class Movie(BaseModel):
	id: Optional[int] = None
	title: str = Field(min_length=5, max_length=50)
	overview: str = Field(min_length=15, max_length=50)
	year: int = Field(le=2022)
	rating: float = Field(ge=1.0,le=10)
	category: str = Field(min_length=5, max_length=50)

	class Config:
		json_schema_extra = {
			'example': {
				'id': 1,
				'title': "Mi Pelicula",
				'overview': "Descripcion de la pelicula",
				'year': "2022",
				'rating': 8.5,
				'category': "Acción"
			}
		}