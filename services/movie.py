from models.movie import Movie as MovieModel
from schemas.movie import Movie as MovieSchema

class MovieService():

    def __init__(self,db) -> None:
        self.db = db

    def get_movies(self):
        result = self.db.query(MovieModel).all()
        return result
    
    def get_movie(self,id):
        result = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        return result
    
    def get_category_movies(self,category:str):
        result = self.db.query(MovieModel).filter(MovieModel.category == category).all()
        return result
    
    def create_movie(self,movie:MovieSchema):
        new_movie = MovieModel(**movie.model_dump())
        self.db.add(new_movie)
        self.db.commit()
        return
    
    def update_movie(self,id:int,movie:MovieSchema):

        result = self.db.query(MovieModel).filter(MovieModel.id == id).first()

        result.title = movie.title
        result.overview = movie.overview
        result.year = movie.year
        result.rating = movie.rating
        result.category = movie.category
        self.db.commit()
        return
    
    def delete_movie(self,id:int):

        self.db.query(MovieModel).filter(MovieModel.id == id).delete()
        self.db.commit()
        return