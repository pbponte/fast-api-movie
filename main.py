from fastapi import  FastAPI
from fastapi.responses import HTMLResponse

# internal
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.auth import auth_router

app = FastAPI()

# Swagger info:
app.title = "My FastAPI App"
app.version = "0.0.3"

# Middlewares
app.add_middleware(ErrorHandler) # t ype: ignore

# Routers
app.include_router(movie_router)
app.include_router(auth_router)

# database init
Base.metadata.create_all(bind=engine)


@app.get('/',tags=['home'])
def message():
  	return HTMLResponse("<h1>Hello World!</h1>")

