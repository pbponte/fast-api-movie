from jwt import encode, decode

jwt_key:str = "pure crap"

def create_token(data:dict):
    token: str = encode(
        payload=data,
        key=jwt_key,
        algorithm="HS256")
    return token

def validate_token(token:str) -> dict:
    token_data:dict = decode(token,key=jwt_key,algorithms=['HS256'])
    return token_data