from fastapi import Depends, Path, Query, APIRouter
from fastapi.responses import  JSONResponse
from typing import List
from fastapi.encoders import jsonable_encoder

from config.database import Session
from middlewares.jwt_bearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter()


## movies
@movie_router.get('/movies',tags=['movies'],response_model=List[Movie],status_code=200,
		 dependencies=[Depends(JWTBearer())])
def get_movies() -> List[Movie] | JSONResponse:
	# creacion de la sesion
	db = Session()
	# consulta de todos los registros
	result = MovieService(db).get_movies()
	return JSONResponse(status_code=200,content=jsonable_encoder(result))

@movie_router.get('/movies/{id}',tags=['movies'], response_model=Movie,status_code=200)
def get_movie(id:int = Path(ge=1,le=2000)) -> Movie | JSONResponse:
	
	db = Session()
	result = MovieService(db).get_movie(id)

	if not result:
		return JSONResponse(status_code=404,content={"message":"No existe esa pelicula"})

	return JSONResponse(status_code=200,content=jsonable_encoder(result))	
	

# si pongo una / al final indica que espero query strings
@movie_router.get('/movies/',tags=['movies'],response_model=List[Movie],status_code=200)
def get_movies_by_category(category: str = Query(min_length=5,max_length=15)) -> List[Movie] | JSONResponse:

	db = Session()
	result = MovieService(db).get_category_movies(category)

	return JSONResponse(status_code=200,content=jsonable_encoder(result))	


@movie_router.post('/movies',tags=['movies'],response_model=dict,status_code=201)
def create_movie(movie: Movie) -> dict | JSONResponse:
	# creo la sesion
	db = Session()
	MovieService(db).create_movie(movie)
	return JSONResponse(status_code=201,content={"message":"Se registró la pelicula"})


@movie_router.put('/movies/{id}',tags=['movies'],response_model=dict,status_code=200)
def update_movie(id:int, movie: Movie) -> dict | JSONResponse:

	db = Session()

	found_movie = MovieService(db).get_movie(id)

	if not found_movie:
		return JSONResponse(status_code=404,content={"message":"No existe esa pelicula"})

	MovieService(db).update_movie(id,movie)
	
	return JSONResponse(status_code=200,content={"message":"Se a modificado la pelicula"})

@movie_router.delete('/movies/{id}',tags=['movies'],response_model=dict,status_code=200)
def delete_movie(id:int) -> dict | JSONResponse:

	db = Session()

	result = MovieService(db).get_movie(id)
	
	if not result:
		return JSONResponse(status_code=404,content={"message":"No existe esa pelicula"})

	MovieService(db).delete_movie(id)
	
	return JSONResponse(status_code=200,content={"message":"Se eliminó la pelicula"})